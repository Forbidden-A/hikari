# Copyright (c) 2020 Nekokatt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

.release:
  allow_failure: false
  before_script:
    - >
      git log -n 1 "${CI_COMMIT_SHA}" --format="%B" \
          | grep -iqE "\[\s*(skip|no|don'?t|do\s+not)\s+deploy(ments?)?\s*\]" \
        && echo "SKIPPING ${CI_JOB_STAGE} STAGE JOB" \
        && exit 0
    # We don't start on the branch, but a ref of it, which isn't useful
    - echo "Changing from ref to actual branch"
    - git fetch -ap
    - git checkout ${TARGET_BRANCH}
    - git reset --hard origin/${TARGET_BRANCH}
    - >
      echo
      echo "========================= hikari/_about.py ========================="
      echo
      cat -n hikari/_about.py
      echo
      echo "===================================================================="
      echo
    - pip install twine wheel nox
  extends: .cpython
  interruptible: false
  needs:
    - test:results
    - install:results
    - lint:results
  resource_group: deploy
  retry: 1
  script:
    - apt-get update
    - apt-get install curl openssh-client -qy

    # Don't dox our keys, or spam echos
    - set +x

    # Init the SSH agent and add our gitlab private key to it.
    - echo ">>>> Starting SSH agent <<<<"
    - eval "$(ssh-agent -s)"
    - echo ">>>> Making ~/.ssh <<<<"
    - mkdir ~/.ssh || true
    - echo ">>>> Writing SSH private key to ~/.ssh/id_rsa <<<<"
    - echo "${GIT_SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa
    - echo ">>>> SSH IS READY <<<<"

    # Be verbose again.
    - set -x

    - chmod 600 ~/.ssh/id_rsa
    - ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
    - ssh-add ~/.ssh/id_rsa

    - nox -s deploy --no-error-on-external-run
  stage: deploy

release:staging:
  environment:
    name: staging
    url: https://nekokatt.gitlab.io/hikari
  extends: .release
  rules:
    - if: "$CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_TAG == null && $CI_COMMIT_REF_NAME == 'staging'"
  variables:
    TARGET_BRANCH: staging

release:master:
  environment:
    name: prod
    url: https://nekokatt.gitlab.io/hikari
  extends: .release
  rules:
    - if: "$CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_TAG == null && $CI_COMMIT_REF_NAME == 'master'"
  variables:
    TARGET_BRANCH: master
